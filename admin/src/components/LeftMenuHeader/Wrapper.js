import styled from 'styled-components';
import PropTypes from 'prop-types';

// import Logo from '../../../assets/images/logo_app.png';

const Wrapper = styled.div`
  background-color: #6FB3B8;
  height: ${props => props.theme.main.sizes.header.height};

  .leftMenuHeaderLink {
      color: #fff!important;
    &:hover {
      text-decoration: none;
      background-color: #388087;
    }
  }

  .projectName {
    display: flex;
    height: 100%;
    width: 100%;
    padding-top: 2rem;
    height: ${props => props.theme.main.sizes.header.height};
    font-size: 2rem;
    justify-content: center;
    align-item: center;
    color: $white;
  }
`;

Wrapper.defaultProps = {
  theme: {
    main: {
      colors: {
        leftMenu: {},
      },
      sizes: {
        header: {},
        leftMenu: {},
      },
    },
  },
};

Wrapper.propTypes = {
  theme: PropTypes.object,
};

export default Wrapper;
